
Table 2 for Docker 
                    Lang-   Lang+
Baseline            0.579l  0.625l  
Word ngrams         0.720   0.723   
POS ngrams          0.722   0.725   
Dependency ngrams   0.699   0.699   
Domain features     0.635   0.696   
Word + char embed.  0.690   0.682   

Table 2 for OsX 
                    Lang-   Lang+
Baseline            0.574l  0.617l  
Word ngrams         0.606   0.607   
POS ngrams          0.680   0.681   
Dependency ngrams   0.651   0.653   
Domain features     0.597   0.647   
Word + char embed.  0.662   0.661   
