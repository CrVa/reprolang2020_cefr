#Adding a document length baseline for final version.

import pprint
import os
import collections
import sys
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score,cross_val_predict,StratifiedKFold 
from sklearn.svm import LinearSVC

import pickle

seed = 1234

def getdoclen(conllufilepath):
    fh =  open(conllufilepath, encoding="utf-8")
    allText = []
    sent_id = 0
    for line in fh:
        if line == "\n":
            sent_id = sent_id+1
        elif not line.startswith("#") and line.split("\t")[3] != "PUNCT":
            word = line.split("\t")[1]
            allText.append(word)
    fh.close()
    return len(allText)

def getfeatures(dirpath):
    mapping = {"A1":1, "A2":2, "B1":3, "B2":4, "C1":5, "C2":6}
    file=open(os.path.join(dirpath,sys.argv[1]+".ord")) 
    files= file.readlines()
    cats = []
    doclenfeaturelist = []
    fileslist = []
    for filename in files:
        filename=filename.rstrip()
        if filename.endswith(".txt"):
            doclenfeaturelist.append([getdoclen(os.path.join(dirpath,filename))]) #mod
            cats.append(mapping[filename.split(".txt")[0].split("_")[-1]])
            fileslist.append(filename)
    return doclenfeaturelist,cats,fileslist

def cgetfeatures(dirpath,code1,code2,code3):
    mapping = {"A1":1, "A2":2, "B1":3, "B2":4, "C1":5, "C2":6}
    file=open(os.path.join(dirpath,sys.argv[1]+".ord")) 
    files= file.readlines()
    cats = []
    doclenfeaturelist = []
    for filename in files:
        filename=filename.rstrip()
        if filename.endswith(".txt"):
            doclenfeaturelist.append([getdoclen(os.path.join(dirpath,filename)),code1,code2,code3]) #mod
            cats.append(mapping[filename.split(".txt")[0].split("_")[-1]])
    return doclenfeaturelist,cats

def singleLangClassificationWithoutVectorizer(train_vector,train_labels,langfiles,tfn):
    pourout = [s.strip(".txt.parsed.txt") for s in langfiles]
    pourout = np.vstack([pourout, train_labels])
    k_fold = StratifiedKFold(10,random_state=seed)
    classifiers = [RandomForestClassifier(class_weight="balanced",n_estimators=300,random_state=seed), LinearSVC(class_weight="balanced",random_state=seed), LogisticRegression(class_weight="balanced",random_state=seed)]
    for classifier in classifiers:
        cross_val = cross_val_score(classifier, train_vector, train_labels, cv=k_fold, n_jobs=1)
        predicted = cross_val_predict(classifier, train_vector, train_labels, cv=k_fold)
        pourout = np.vstack([pourout, predicted])
    with open(tfn, "wb") as fp:
        pickle.dump(pourout.T, fp)
    
def myfonct():
    global tfn
    itdirpath = "input/IT-Parsed"
    dedirpath = "input/DE-Parsed"
    czdirpath = "input/CZ-Parsed"

    defeats,delabels,lang1files = getfeatures(dedirpath)
    itfeats,itlabels,lang2files = getfeatures(itdirpath)
    czfeats,czlabels,lang3files = getfeatures(czdirpath)
    bigfeats = []
    bigcats = []
    bigfeats.extend(defeats)
    bigfeats.extend(itfeats)
    bigfeats.extend(czfeats)
    bigcats.extend(delabels)
    bigcats.extend(itlabels)
    bigcats.extend(czlabels)
    megafile=lang1files + lang2files + lang3files
    
    tfn="output/datasets/T2_FA_"+sys.argv[1]+"_B.bin"
    singleLangClassificationWithoutVectorizer(bigfeats,bigcats,megafile,tfn)

    defeats,delabels = cgetfeatures(dedirpath,1,0,0)
    itfeats,itlabels = cgetfeatures(itdirpath,0,1,0)
    czfeats,czlabels = cgetfeatures(czdirpath,0,0,1)
    bigfeats = []
    bigcats = []
    bigfeats.extend(defeats)
    bigfeats.extend(itfeats)
    bigfeats.extend(czfeats)
    bigcats.extend(delabels)
    bigcats.extend(itlabels)
    bigcats.extend(czlabels)
    
    tfn="output/datasets/T2_TR_"+sys.argv[1]+"_B.bin"
    singleLangClassificationWithoutVectorizer(bigfeats,bigcats,megafile,tfn)
    
def main():
     myfonct()
    
if __name__ == "__main__":
    main()

