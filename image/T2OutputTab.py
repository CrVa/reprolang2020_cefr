#Adding a document length baseline for final version.

import pprint
import os
import collections
import numpy as np
from sklearn.metrics import f1_score

import pickle
import sys
from scipy import stats



def permtest(fn1,fn2):
    global text_file
    with open(fn1, "rb") as fp:   # Unpickling
        data1 = pickle.load(fp)
    with open(fn2, "rb") as fp:   # Unpickling
        data2 = pickle.load(fp)

    if (data1[:,0]!=data2[:,0]).all():
        print("les reps sont differentes... End")
        return -999999
    f1a = f1_score(data1[:,0],data1[:,1],average='weighted')
    f2a = f1_score(data2[:,0],data2[:,1],average='weighted')
    text_file.write("%5.3lf %5.3lf " % (f1a,f2a))
    d=time.time()
    #print(rand_permutation(data[:,0],data[:,1],data[:,2], 1000))
    text_file.write("%8.6lf\n" % ma_permf1(data1[:,0],data1[:,1],data2[:,1], 1000))
    print("Temps total = %6.2lf\n" % (time.time() - d))
    
def ajoulet(text_file,w):
    if w == 0:
        return("  ")
    elif w == 1:
        return("s ")
    else:
        return("l ")
    
def trouvemax(res_single):
    mymax=max(res_single[0],res_single[1],res_single[2])
    if res_single[0] == mymax:
       return(0)
    elif res_single[1] == mymax:
       return(1)
    else:
       return(2);
        
def main():
    res_single = np.zeros(3)
    myos=["Docker","OsX"]
    co=["B","W","P","D","o","N"]
    mapping = {"B":"Baseline          ","W":"Word ngrams       ","P":"POS ngrams        ","D":"Dependency ngrams ","o":"Domain features   ","N":"Word + char embed."} #"A1":1, "A2":2, "B1":3, "B2":4, "C1":5, "C2":6}
    labelascat=["FA","TR"]
    #Document order OK?
    with open("output/datasets/T2_FA_Docker_B.bin", "rb") as fp:   # Unpickling
     data1 = pickle.load(fp)
    
    text_file = open("output/tables_and_plots/ResTab2.txt", "w")
    for myos1 in myos:
      text_file.write("\nTable 2 for %s \n" % myos1) #: B = Baseline, W = Word n-grams, P = POS n-grams, D = Dep n-grams, o = Domain features, N = Neural model\n" % myos1)
      text_file.write("                    Lang-   Lang+\n")
      for co1 in co:
        text_file.write("%s  " % mapping[co1])
        for la in labelascat:
          with open("output/datasets/T2_"+la+"_"+myos1+"_"+co1+".bin", "rb") as fp:   # Unpickling
            data = pickle.load(fp)
          if (data1[:,0]!=data[:,0]).all():
            print("Instances are not in the same order in the two files. End. End")
            print(data1[:,0])
            print(data[:,0])
            return -999999          
          if co1 != "N":
            for j in range(2,5):
              res_single[j-2]=f1_score(data[:,1],data[:,j],average='weighted')
            pmax=trouvemax(res_single)
            text_file.write("%5.3lf%s " % (res_single[pmax],ajoulet(text_file,pmax)))
          else:
           text_file.write("%5.3lf   " % f1_score(data[:,1],data[:,2],average='weighted'))
        text_file.write("\n")

main()

