#Adding a document length baseline for final version.

import pprint
import os
import collections
import numpy as np
#from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor, RandomForestClassifier, RandomForestRegressor
#from sklearn.linear_model import LogisticRegression, LinearRegression
#from sklearn.pipeline import Pipeline
#from sklearn.model_selection import cross_val_score,cross_val_predict,StratifiedKFold 
from sklearn.metrics import f1_score,classification_report,accuracy_score,confusion_matrix, mean_absolute_error
#from sklearn.svm import LinearSVC

from scipy.stats import spearmanr, pearsonr, t

#import statistics 
from statistics import stdev, mean
from math import sqrt

#from xgboost import XGBClassifier, XGBRegressor

import pickle
import time
import sys
from scipy import stats


np.random.seed(1234)

#global variable for output
res_single = np.zeros((3,3))
colonne=0
ligne=0
w0=0
w1=0
w2=0

def ma_permf1(rep,data_A, data_B,R,diff_obs):
    n=len(rep)
    r = 0
    temp_A = data_A
    temp_B = data_B
    for x in range(0, R):
        rnd = np.random.randint(2, size=n)
        for i in range(0, n-1):
            if rnd[i]:
                temp_B[i], temp_A[i] = temp_A[i], temp_B[i]
        if (abs(f1_score(rep,temp_A,average='weighted')-f1_score(rep,temp_B,average='weighted')) >= diff_obs):
            r += 1
    return float(r+1.0)/(R+1.0)
    
def permtest(fnb,co):
    global text_file
    mapping = {"W":"Word ngrams","B":"Baseline","P":"POS ngrams","D":"Dependency ngrams",
               "o":"Domain features","Wo":"Word ng. + domain","Po":"POS ng. + domain",
               "Do":"Dep. ng. + domain","N":"Word embeddings"} 
    #n=len(co)
    f1val=[];
    maxf1=-1.0
    #on calcule les moyennes et trouve le max
    for co1 in co:   
      mf1=0   
      for which in range(0,10):
        fn1=(fnb +  str(which) + "_" + co1 + ".bin")  
        with open(fn1, "rb") as fp:   # Unpickling
          data1 = pickle.load(fp)
        mf1+=f1_score(data1[:,1],data1[:,2],average='weighted')
      f1val.append(mf1)
      if (mf1>maxf1):
        maxf1=mf1
        wmax=co1
      #print(co1, mf1)
    #print(wmax, maxf1) 
    n=-1;
    for co1 in co:   
      n+=1
      if (wmax != co1):
        nns=0
        n05=0
        nlt=0
        for which in range(0,10):
          fn1=(fnb + str(which) + "_"  + wmax + ".bin")  
          with open(fn1, "rb") as fp:   # Unpickling
            data1 = pickle.load(fp)
          fn1=(fnb + str(which) + "_"  + co1 + ".bin")  
          with open(fn1, "rb") as fp:   # Unpickling
            data2 = pickle.load(fp)
          diff_obs = f1_score(data1[:,1],data1[:,2],average='weighted')-f1_score(data1[:,1],data2[:,2],average='weighted')
          if (diff_obs<=0):
            nlt+=1
            nns+=1
            n05+=1 
          else: #if (nns==0 or n05==0):
            p=ma_permf1(data1[:,1],data1[:,2],data2[:,2], 2000,abs(diff_obs))
            if (p>0.05):
              nns+=1
              n05+=1             
            elif (p>0.005):
              nns+=1;
        text_file.write("%s %s  %5.3lf %5.3lf %2ld %2ld\n" % (mapping[wmax],mapping[co1],maxf1/10,f1val[n]/10,10-n05,10-nlt))

def main():
  global text_file
  res_single = np.zeros(10)
  la=["DE","IT","CZ"]
  co=["B","W","P","D","o","Wo","Po","Do","N"]
  mapping = {"W":"Word ngrams       ","B":"Baseline          ","P":"POS ngrams        ","D":"Dependency ngrams ",
             "o":"Domain features   ","Wo":"Word ng. + domain ","Po":"POS ng. + domain  ",
             "Do":"Dep. ng. + domain ","N":"Word embeddings   "} 
  text_file = open("output/tables_and_plots/ResTab3.txt", "w")
  with open("output/datasets/T3_DE_0_P.bin", "rb") as fp:   # Unpickling
    datade1 = pickle.load(fp)    
  with open("output/datasets/T3_IT_0_P.bin", "rb") as fp:   # Unpickling
    datait1 = pickle.load(fp)    
  with open("output/datasets/T3_CZ_0_P.bin", "rb") as fp:   # Unpickling
    datacz1 = pickle.load(fp)    
  text_file.write("\nTable 3\n")
  text_file.write("                         DE           IT           CZ\n")
  for co1 in co:
    text_file.write("%s  " % mapping[co1])
    for la1 in la:
      for which in range(10):
        with open("output/datasets/T3_"+la1+"_"+str(which)+"_"+co1+".bin", "rb") as fp:   # Unpickling
          data = pickle.load(fp)
        if la1=="DE":
          if (datade1[:,0]!=data[:,0]).all():
            print("Instances are not in the same order in the two files. End.")
            return -999999  
        elif la1=="IT":
          if (datait1[:,0]!=data[:,0]).all():
            print("Instances are not in the same order in the two files. End.")
            return -999999  
        else:
          if (datacz1[:,0]!=data[:,0]).all():
            print("Instances are not in the same order in the two files. End.")
            return -999999  
        res_single[which]=f1_score(data[:,1],data[:,2],average='weighted')
      #text_file.write("%5.3lf %5.3lf %5.3lf  " % (min(res_single),mean(res_single),max(res_single)))
      text_file.write("%5.3lf %5.3lf  " % (mean(res_single),max(res_single)-min(res_single)))
    text_file.write("\n")
  text_file.close()
  
  text_file = open("output/tables_and_plots/ResTab4.txt", "w")  
  for la1 in la:
    text_file.write("Significance tests for %s\n" % (la1))
    fnb=("output/datasets/T3_"+la1+"_") 
    permtest(fnb,co)
  
main()

