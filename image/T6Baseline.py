#Adding a document length baseline for final version.

import pprint
import os
import collections
import numpy as np
from sklearn.ensemble import GradientBoostingClassifier, GradientBoostingRegressor, RandomForestClassifier, RandomForestRegressor
from sklearn.linear_model import LogisticRegression, LinearRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score,cross_val_predict,StratifiedKFold 
from sklearn.svm import LinearSVC

from scipy.stats import spearmanr, pearsonr
#from xgboost import XGBClassifier, XGBRegressor

import pickle

seed = 1234


def getdoclen(conllufilepath):
    fh =  open(conllufilepath, encoding="utf-8")
    allText = []
    sent_id = 0
    for line in fh:
        if line == "\n":
            sent_id = sent_id+1
        elif not line.startswith("#") and line.split("\t")[3] != "PUNCT":
            word = line.split("\t")[1]
            allText.append(word)
    fh.close()
    return len(allText)

def getfeatures(dirpath):
    mapping = {"A1":1, "A2":2, "B1":3, "B2":4, "C1":5, "C2":6}
    file=open(os.path.join(dirpath,"Docker.ord"))
    #files = os.listdir(dirpath)
    files= file.readlines()
    cats = []
    doclenfeaturelist = []
    fileslist = []
    for filename in files:
        filename=filename.rstrip()
        if filename.endswith(".txt"):
            doclenfeaturelist.append([getdoclen(os.path.join(dirpath,filename))]) #mod
            cats.append(mapping[filename.split(".txt")[0].split("_")[-1]])
            fileslist.append(filename)
    return doclenfeaturelist,cats,fileslist

def cgetfeatures(dirpath,code1,code2,code3):
    mapping = {"A1":1, "A2":2, "B1":3, "B2":4, "C1":5, "C2":6}
    file=open(os.path.join(dirpath,"Docker.ord"))
    #files = os.listdir(dirpath)
    files= file.readlines()
    cats = []
    doclenfeaturelist = []
    for filename in files:
        filename=filename.rstrip()
        if filename.endswith(".txt"):
            doclenfeaturelist.append([getdoclen(os.path.join(dirpath,filename)),code1,code2,code3]) #mod
            cats.append(mapping[filename.split(".txt")[0].split("_")[-1]])
    return doclenfeaturelist,cats

def crossLangClassificationWithoutVectorizer(train_vector, train_labels, test_vector, test_labels,langfiles,tfn):
    classifiers = [RandomForestClassifier(class_weight="balanced",n_estimators=300,random_state=seed), LinearSVC(class_weight="balanced",random_state=seed), LogisticRegression(class_weight="balanced",random_state=seed)]
    pourout = [s.strip(".txt.parsed.txt") for s in langfiles]
    pourout = np.vstack([pourout, test_labels])
    for classifier in classifiers:
        classifier.fit(train_vector,train_labels)
        predicted = classifier.predict(test_vector)
        pourout = np.vstack([pourout, predicted])
    with open(tfn, "wb") as fp:
        pickle.dump(pourout.T, fp)

def crossLangRegressionWithoutVectorizer(train_vector, train_scores, test_vector, test_scores,langfiles,tfn):
    regressors = [LinearRegression(n_jobs=1), RandomForestRegressor(random_state=seed,n_jobs=1), GradientBoostingRegressor(random_state=seed)] #[RandomForestRegressor()]
    pourout = [s.strip(".txt.parsed.txt") for s in langfiles]
    pourout = np.vstack([pourout, test_scores])
    for regressor in regressors:
      regressor.fit(train_vector,train_scores)
      predicted =regressor.predict(test_vector)
      predicted[predicted < 0] = 0
      pourout = np.vstack([pourout, predicted])
    with open(tfn, "wb") as fp:
      pickle.dump(pourout.T, fp)

def myfonct():
    itdirpath = "input/IT-Parsed"
    dedirpath = "input/DE-Parsed"
    czdirpath = "input/CZ-Parsed"
     
    defeats,delabels,delangfiles = getfeatures(dedirpath)
    itfeats,itlabels,itlang1files = getfeatures(itdirpath)
    czfeats,czlabels,czlang1files = getfeatures(czdirpath)

    print("*** Train with DE, test with IT baseline******")
    tfn="output/datasets/T6_CL_DEIT_B.bin"
    crossLangClassificationWithoutVectorizer(defeats,delabels, itfeats,itlabels,itlang1files,tfn)
    tfn="output/datasets/T6_RE_DEIT_B.bin"
    crossLangRegressionWithoutVectorizer(defeats,delabels, itfeats,itlabels,itlang1files,tfn)
 
    print("*** Train with DE, test with CZ baseline ******")
    tfn="output/datasets/T6_CL_DECZ_B.bin"
    crossLangClassificationWithoutVectorizer(defeats,delabels, czfeats,czlabels,czlang1files,tfn)
    tfn="output/datasets/T6_RE_DECZ_B.bin"
    crossLangRegressionWithoutVectorizer(defeats,delabels, czfeats,czlabels,czlang1files,tfn)

def main():
    myfonct() # pour T6
    
if __name__ == "__main__":
    main()

