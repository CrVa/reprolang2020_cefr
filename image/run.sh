#!/bin/bash

echo "Hello GitLab"

#For Table 2
python3 T2Baseline.py OsX > /output/tables_and_plots/Log.txt
python3 T2Baseline.py Docker >> /output/tables_and_plots/Log.txt
python3 T2Feat.py OsX >> /output/tables_and_plots/Log.txt
python3 T2Feat.py Docker >> /output/tables_and_plots/Log.txt
python3 T2NeuralFA.py Docker >> /output/tables_and_plots/Log.txt
python3 T2NeuralTR.py Docker >> /output/tables_and_plots/Log.txt
python3 T2NeuralFA.py OsX >> /output/tables_and_plots/Log.txt
python3 T2NeuralTR.py OsX >> /output/tables_and_plots/Log.txt
python3 T2OutputTab.py >> /output/tables_and_plots/Log.txt


#For Table 1, 3, 4 and 5
python3 T3T5Baseline.py >> /output/tables_and_plots/Log.txt
python3 T3T5Feat.py >> /output/tables_and_plots/Log.txt
python3 T3Embed.py >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 0 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 1 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 2 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 3 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 4 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 5 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 6 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 7 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 8 >> /output/tables_and_plots/Log.txt
python3 T5EmbedTR.py 9 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 0 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 1 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 2 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 3 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 4 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 5 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 6 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 7 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 8 >> /output/tables_and_plots/Log.txt
python3 T5EmbedFA.py 9 >> /output/tables_and_plots/Log.txt
python3 T3OutputTab.py >> /output/tables_and_plots/Log.txt
python3 T5OutputTab.py >> /output/tables_and_plots/Log.txt
python3 OutputTab1.py >> /output/tables_and_plots/Log.txt


#For Table 6 and 7
python3 T6Baseline.py >> /output/tables_and_plots/Log.txt
python3 T6Class.py >> /output/tables_and_plots/Log.txt
python3 T6Reg.py >> /output/tables_and_plots/Log.txt
python3 T6OutputTab.py >> /output/tables_and_plots/Log.txt


echo "ByeBye GitLab"
