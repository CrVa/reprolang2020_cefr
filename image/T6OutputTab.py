#Adding a document length baseline for final version.

import pprint
import os
import collections
import numpy as np
from sklearn.metrics import f1_score
from scipy.stats import spearmanr, pearsonr

import pickle
import sys
from scipy import stats

np.random.seed(1234)

def ma_permf1(rep,data_A, data_B, R):
    n=len(rep)
    diff_obs = abs(f1_score(rep,data_A,average='weighted')-f1_score(rep,data_B,average='weighted'))
    r = 0
    temp_A = data_A
    temp_B = data_B
    for x in range(0, R):
        rnd = np.random.randint(2, size=n)
        for i in range(0, n-1):
            if rnd[i]:
                temp_B[i], temp_A[i] = temp_A[i], temp_B[i]
        if (abs(f1_score(rep,temp_A,average='weighted')-f1_score(rep,temp_B,average='weighted')) >= diff_obs):
            r += 1
    return float(r+1.0)/(R+1.0)

def ma_permr(rep,data_A, data_B, R):
    n=len(rep)
    diff_obs = abs(pearsonr(rep,data_A)[0]-pearsonr(rep,data_B)[0])
    r = 0
    temp_A = data_A
    temp_B = data_B
    for x in range(0, R):
        rnd = np.random.randint(2, size=n)
        for i in range(0, n-1):
            if rnd[i]:
                temp_B[i], temp_A[i] = temp_A[i], temp_B[i]
        if (abs(pearsonr(rep,temp_A)[0]-pearsonr(rep,temp_B)[0]) >= diff_obs):
            r += 1
    return float(r+1.0)/(R+1.0)
    
def ajoulet(text_file,w):
    if w == 0:
        return("  ")
    elif w == 1:
        return("s ")
    else:
        return("l ")
def ajoulet3(text_file,w):
    if w == 0:
        return("l ")
    elif w == 1:
        return("r ")
    else:
        return("  ")
    
def trouvemax(res_single):
    mymax=max(res_single[0],res_single[1],res_single[2])
    if res_single[0] == mymax:
       return(0)
    elif res_single[1] == mymax:
       return(1)
    else:
       return(2);

def trouvemax4(res_single):
    mymax=max(res_single[0],res_single[1],res_single[2],res_single[3])
    if res_single[0] == mymax:
       return(0)
    elif res_single[1] == mymax:
       return(1)
    elif res_single[2] == mymax:
       return(2)
    else:
       return(3);

def permtest(fn1,p1,fn2,p2):
    global text_file
    with open(fn1, "rb") as fp:   # Unpickling
        data1 = pickle.load(fp)
    with open(fn2, "rb") as fp:   # Unpickling
        data2 = pickle.load(fp)

    f1a = f1_score(data1[:,1],data1[:,p1],average='weighted')
    f2a = f1_score(data2[:,1],data2[:,p2],average='weighted')
    text_file.write("%5.3lf %5.3lf " % (f1a,f2a))
    #print(rand_permutation(data[:,0],data[:,1],data[:,2], 1000))
    text_file.write("%8.6lf\n" % ma_permf1(data1[:,1],data1[:,p1],data2[:,p2], 2000))

def permtestr(fn1,p1,fn2,p2):
    global text_file
    with open(fn1, "rb") as fp:   # Unpickling
        mdata = pickle.load(fp)
    data1=[[(n) for [n,y,x1,x2,x3] in mdata],
            [(int(y)) for [n,y,x1,x2,x3] in mdata], [float(x1) for [n,y,x1,x2,x3] in mdata],
            [float(x2) for [n,y,x1,x2,x3] in mdata], [float(x3) for [n,y,x1,x2,x3] in mdata]]
    with open(fn2, "rb") as fp:   # Unpickling
        mdata = pickle.load(fp)
    data2=[[(n) for [n,y,x1,x2,x3] in mdata],
            [(int(y)) for [n,y,x1,x2,x3] in mdata], [float(x1) for [n,y,x1,x2,x3] in mdata],
            [float(x2) for [n,y,x1,x2,x3] in mdata], [float(x3) for [n,y,x1,x2,x3] in mdata]]
    
    f1a = pearsonr(data1[1],data1[p1])[0]
    f2a = pearsonr(data2[1],data2[p2])[0]
    text_file.write("%5.3lf %5.3lf " % (f1a,f2a))
    #print(rand_permutation(data[:,0],data[:,1],data[:,2], 1000))
    text_file.write("%8.6lf\n" % ma_permr(data1[1],data1[p1],data2[p2], 2000))
        
def main():
  global text_file
  posmax=[]
  res_single = np.zeros(4)
  co=["B","P","D","o"]
  mapping = {"B":"Baseline          ","P":"POS ngrams        ","D":"Dependency ngrams ","o":"Domain features   ","N":"Word + char embed."} #"A1":1, "A2":2, "B1":3, "B2":4, "C1":5, "C2":6}
  mappings = {"B":"Baseline","P":"POS ngrams","D":"Dependency ngrams","o":"Domain features","N":"Word + char embed."} #"A1":1, "A2":2, "B1":3, "B2":4, "C1":5, "C2":6}
  la=["IT","CZ"]
  
  
  text_file = open("output/tables_and_plots/ResTab6.txt", "w")
  
  with open("output/datasets/T6_CL_DEIT_P.bin", "rb") as fp:   # Unpickling
    datait1 = pickle.load(fp)    
  with open("output/datasets/T6_CL_DECZ_P.bin", "rb") as fp:   # Unpickling
    datacz1 = pickle.load(fp)    
  text_file.write("\nTable 6 (for Classification) \n")
  text_file.write("                     IT      CZ\n")
  for co1 in co:
    text_file.write("%s  " % mapping[co1])
    for la1 in la:
      with open("output/datasets/T6_CL_DE"+la1+"_"+co1+".bin", "rb") as fp:   # Unpickling
        data = pickle.load(fp)
      if la1=="IT":
        if (datait1[:,0]!=data[:,0]).all():
          print("Instances are not in the same order in the two files. End.")
          print(data1[:,0])
          print(data[:,0])
          return -999999  
      else:
        if (datacz1[:,0]!=data[:,0]).all():
          print("Instances are not in the same order in the two files. End.")
          print(data1[:,0])
          print(data[:,0])
          return -999999  
      for j in range(2,5):
        res_single[j-2]=f1_score(data[:,1],data[:,j],average='weighted')
      pmax=trouvemax(res_single)
      posmax.append(pmax)
      text_file.write("%5.3lf%s " % (res_single[pmax],ajoulet(text_file,pmax)))
    text_file.write("\n")
  print(posmax)  
  mod=-1
  for la1 in la:
    mod+=1
    text_file.write("Results for %s\n" % (la1))
    for co1 in list(range(3)):
      #print(co1)     
      for co2 in list(range(co1+1,4)):
        #print(co2)
        print("%2s %1c %1c " % (la1,co[co1],co[co2]))
        fn1=("output/datasets/T6_CL_DE"+la1+"_"+co[co1]+".bin")
        fn2=("output/datasets/T6_CL_DE"+la1+"_"+co[co2]+".bin")
        text_file.write("%s %s " % (mappings[co[co1]],mappings[co[co2]]))
        permtest(fn1,posmax[co1*2+mod]+2,fn2,posmax[co2*2+mod]+2)
  text_file.close()
  
  text_file = open("output/tables_and_plots/ResTab7.txt", "w")  
  posmax=[]  
  with open("output/datasets/T6_RE_DEIT_P.bin", "rb") as fp:   # Unpickling
    datait1 = pickle.load(fp)    
  with open("output/datasets/T6_RE_DECZ_P.bin", "rb") as fp:   # Unpickling
    datacz1 = pickle.load(fp)    
  text_file.write("\nTable 7 (for Regression) \n")
  text_file.write("                     IT      CZ\n")
  for co1 in co:
    text_file.write("%s  " % mapping[co1])
    for la1 in la:
      with open("output/datasets/T6_RE_DE"+la1+"_"+co1+".bin", "rb") as fp:   # Unpickling
        mdata = pickle.load(fp)
      if la1=="IT":
        if (datait1[:,0]!=mdata[:,0]).all():
          print("Instances are not in the same order in the two files. End.")
          print(data1[:,0])
          print(mdata[:,0])
          return -999999  
      else:
        if (datacz1[:,0]!=mdata[:,0]).all():
          print("Instances are not in the same order in the two files. End.")
          print(data1[:,0])
          print(mdata[:,0])
          return -999999  
      #print(data)
      data=[[(n) for [n,y,x1,x2,x3] in mdata],
            [(int(y)) for [n,y,x1,x2,x3] in mdata], [float(x1) for [n,y,x1,x2,x3] in mdata],
            [float(x2) for [n,y,x1,x2,x3] in mdata], [float(x3) for [n,y,x1,x2,x3] in mdata]]
      #print(data[1])   
      for j in range(2,5):
        res_single[j-2]=pearsonr(data[1],data[j])[0]
      pmax=trouvemax(res_single)
      posmax.append(pmax)
      text_file.write("%5.3lf%s " % (res_single[pmax],ajoulet3(text_file,pmax)))
    text_file.write("\n")

  mod=-1
  for la1 in la:
    mod+=1
    text_file.write("Significance tests for %s\n" % (la1))
    for co1 in list(range(3)):
      #print(co1)     
      for co2 in list(range(co1+1,4)):
        #print(co2)
        print("%2s %1c %1c " % (la1,co[co1],co[co2]))
        fn1=("output/datasets/T6_RE_DE"+la1+"_"+co[co1]+".bin")
        fn2=("output/datasets/T6_RE_DE"+la1+"_"+co[co2]+".bin")
        text_file.write("%s %s " % (mappings[co[co1]],mappings[co[co2]]))
        permtestr(fn1,posmax[co1*2+mod]+2,fn2,posmax[co2*2+mod]+2)

main()

