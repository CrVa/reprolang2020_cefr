#from __future__ import print_function
import pprint
import os
import collections
import sys
import numpy as np
np.random.seed(1337)  # for reproducibility


from keras.preprocessing import sequence
from keras.models import Sequential, Model
from keras.layers import Dense, Flatten, Dropout, Activation, Input, merge
from keras.layers import Embedding, Convolution1D, MaxPooling1D
from keras.layers import AveragePooling1D, LSTM, GRU
from keras.utils import np_utils
import sys, time, re, glob
from collections import defaultdict
from gensim.utils import simple_preprocess
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import f1_score, confusion_matrix

import pickle

_white_spaces = re.compile(r"\s\s+")

maxchars = 200
embedding_dims = 100
batch_size = 32
nb_epoch = 10
nb_filter = 128
filter_length = 5
pool_length = 32
minfreq = 0
#data_path = sys.argv[1]
minwordfreq = 15
maxwordlen = 400
seed = 1234

import keras.backend as K

def mean_pred(y_true, y_pred):
    return K.mean(y_pred)


def read_data(la):
    fileslist=[]
    i=0
    labels = []
    documents = []
    file=open(os.path.join("input/"+la,"Docker.ord")) #"VerifOrd.txtt")) #
    files= file.readlines()
    for data_file in files:
        data_file=data_file.rstrip()
        if ".txt" not in data_file: continue
        doc = open(os.path.join("input/"+la,data_file), "r").read().strip()
        wrds = doc.split(" ")
        label = data_file.split("/")[-1].split(".txt")[0].split("_")[-1]
        if label == "EMPTY": continue
        if len(wrds) >= maxwordlen:
            doc = " ".join(wrds[:maxwordlen])
        doc = _white_spaces.sub(" ", doc)
        labels.append(label)
        documents.append(doc)
        fileslist.append([data_file.strip(".txt"),i])
        i+=1
    return (documents, labels, fileslist)

def char_tokenizer(s):
    return list(s)

def word_tokenizer(s):
    return simple_preprocess(s)

def getWords(D):
    wordSet = defaultdict(int)
    max_features = 3
    for d in D:
        for c in word_tokenizer(d):
            wordSet[c] += 1
    for c in wordSet:
        if wordSet[c] > minwordfreq:
            max_features += 1
    return wordSet, max_features

def getChars(D):
    charSet = defaultdict(int)
    max_features = 3
    for d in D:
        for c in char_tokenizer(d):
            charSet[c] += 1
    for c in charSet:
        if charSet[c] > minfreq:
            max_features += 1
    return charSet, max_features

def transform(D, vocab, minfreq, tokenizer="char"):
    features = defaultdict(int)
    count = 0
    for i, k in enumerate(vocab.keys()):
        if vocab[k] > minfreq:
            features[k] = count
            count += 1
    
    start_char = 1
    oov_char = 2
    index_from = 3
    
    X = []
    for j, d in enumerate(D):
        x = [start_char]
        z = None
        if tokenizer == "word":
            z = word_tokenizer(d)
        else:
            z = char_tokenizer(d)
        for c in z:
            freq = vocab[c]
            if c in vocab:
                if c in features:
                    x.append(features[c]+index_from)
                else:
                    x.append(oov_char)
            else:
                continue
        X.append(x)
    return X

def myfonct(which,la):    
    global seed
    sys.stdout.flush()
    pt = time.time()
    doc_train, y_labels, fileslist = read_data(la)
    
    sys.stdout.flush()
    pt = time.time()
    
    word_vocab, max_word_features = getWords(doc_train)
    x_word_train = transform(doc_train, word_vocab, minwordfreq, tokenizer="word")
    
    x_word_train = sequence.pad_sequences(x_word_train, maxlen=maxwordlen)
    sys.stdout.flush()
    pt = time.time()
    unique_labels = list(set(y_labels))
    n_classes = len(unique_labels)
    indim = x_word_train.shape[1]
    
    y_labels = [unique_labels.index(y) for y in y_labels]
    
    y_train = np_utils.to_categorical(np.array(y_labels), len(unique_labels))
    
    cv_accs, cv_f1 = [], []
    k_fold = StratifiedKFold(10, shuffle=True,random_state=seed)
    all_gold = []
    all_preds = []
    all_filename = []
    for train, test in k_fold.split(x_word_train, y_labels):
        model = Sequential()
        model.add(Embedding(max_word_features, embedding_dims, input_length=maxwordlen))
        model.add(Flatten())
        model.add(Dense(n_classes, activation='softmax'))
        model.summary()
        model.compile(loss='categorical_crossentropy',
                    optimizer='adadelta',
                    metrics=['accuracy'])
        model.fit(x_word_train[train], y_train[train],
                  batch_size=batch_size,
                  epochs=nb_epoch)
        y_pred = model.predict_classes(x_word_train[test])
        pred_labels = [unique_labels[x] for x in y_pred]
        gold_labels = [unique_labels[x] for x in np.array(y_labels)[test]]
        all_gold.extend(gold_labels)
        all_preds.extend(pred_labels)
        temp = [fileslist[x] for x in test]
        all_filename.extend(temp)
        cv_f1.append(f1_score(np.array(y_labels)[test], y_pred, average="weighted"))
    
    print("\nF1-scores", cv_f1,sep="\n")
    print("Average F1 scores", np.mean(cv_f1))
    print("Global F1 scores", f1_score(all_gold, all_preds, average="weighted"))
    idx = [y for [x,y] in all_filename]
    sall_gold = [x for _,x in sorted(zip(idx,all_gold))]    
    sall_preds = [x for _,x in sorted(zip(idx,all_preds))]    
    sall_filename = [x for _,x in sorted(zip(idx,all_filename))]    
    pourout = np.vstack([[x for x,_ in sall_filename],sall_gold, sall_preds])     
    with open("output/datasets/T3_"+la+"_"+str(which)+"_N.bin", "wb") as fp:
        pickle.dump(pourout.T, fp)

def main():
    global seed
    all_my_seed=[528971,198243,327801,985204,267314,797340,412097,608921,810582,132469,692310,438976,147902,218952,368270,410746,595218,690402,748392,853914]
    lan=["DE","IT","CZ"]
    for la in lan:
        for which in range(10):
            seed=all_my_seed[which]
            myfonct(which,la)
    
if __name__ == "__main__":
    main()
    