#For Table 1

import pprint
import os
import collections
import numpy as np

import pickle
import time
import sys


def main():

  text_file = open("output/tables_and_plots/ResTab1.txt", "w")
  with open("output/datasets/T3_DE_0_P.bin", "rb") as fp:   # Unpickling
    datade = pickle.load(fp)    
  with open("output/datasets/T3_IT_0_P.bin", "rb") as fp:   # Unpickling
    datait = pickle.load(fp)    
  with open("output/datasets/T3_CZ_0_P.bin", "rb") as fp:   # Unpickling
    datacz = pickle.load(fp)    
  text_file.write("\nTable 1 : Mapping : 1=A1 2=A2 3=B1 4=B2 5=C1\n")
  
  unique_elements, counts_elements = np.unique(datade[:,1], return_counts=True)
  text_file.write("DE\n")
  text_file.write(str(np.asarray((unique_elements, counts_elements))))
  unique_elements, counts_elements = np.unique(datait[:,1], return_counts=True)
  text_file.write("\nIT\n")
  text_file.write(str(np.asarray((unique_elements, counts_elements))))
  unique_elements, counts_elements = np.unique(datacz[:,1], return_counts=True)
  text_file.write("\nCZ\n")
  text_file.write(str(np.asarray((unique_elements, counts_elements))))
  
main()

