#Purpose: Build a scorer with POS N-grams. Use it on another language.

import pprint
import os
import collections
import sys
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import Imputer #to replace NaN with mean values.
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score,cross_val_predict,StratifiedKFold 
from sklearn.svm import LinearSVC

import language_check

import pickle
tfn="monfichier"

seed=1234


'''
convert a text into its POS form. i.e., each word is replaced by its POS
'''
def makePOSsentences(conllufilepath):
    fh =  open(conllufilepath)
    everything_POS = []

    pos_sentence = []
    sent_id = 0
    for line in fh:
        if line == "\n":
            pos_string = " ".join(pos_sentence) + "\n"
            everything_POS.append(pos_string)
            pos_sentence = []
            sent_id = sent_id+1
        elif not line.startswith("#"):
            pos_tag = line.split("\t")[3]
            pos_sentence.append(pos_tag)
    fh.close()
    return " ".join(everything_POS) #Returns a string which contains one sentence as POS tag sequence per line

def makeTextOnly(conllufilepath):
    fh =  open(conllufilepath)
    allText = []
    this_sentence = []
    sent_id = 0
    for line in fh:
        if line == "\n":
            word_string = " ".join(this_sentence) + "\n"
            allText.append(word_string)
            this_sentence = []
            sent_id = sent_id+1
        elif not line.startswith("#"):
            word = line.split("\t")[1]
            this_sentence.append(word)
    fh.close()
    return " ".join(allText) #Returns a string which contains one sentence as POS tag sequence per line

'''
convert a sentence into this form: nmod_NN_PRON, dobj_VB_NN etc. i.e., each word is replaced by a dep. trigram of that form.
So full text will look like this instead of a series of words or POS tags:
root_X_ROOT punct_PUNCT_X case_ADP_PROPN nmod_PROPN_X flat_PROPN_PROPN
 root_PRON_ROOT nsubj_NOUN_PRON case_ADP_PROPN det_DET_PROPN nmod_PROPN_NOUN
 case_ADP_NOUN det_DET_NOUN nummod_NUM_NOUN obl_NOUN_VERB root_VERB_ROOT case_ADP_NOUN det_DET_NOUN obl_NOUN_VERB appos_PROPN_NOUN flat_PROPN_PROPN case_ADP_NOUN obl_NOUN_VERB cc_CCONJ_PART conj_PART_PROPN punct_PUNCT_VERB
 advmod_ADJ_VERB case_ADP_VERB case_ADP_VERB nmod_NOUN_ADP case_ADP_VERB nmod_NOUN_ADP case_ADP_VERB det_DET_NUM obl_NUM_VERB root_VERB_ROOT punct_PUNCT_VERB
 root_PRON_ROOT obj_NOUN_PROPN det_DET_PROPN amod_PROPN_PRON cc_CCONJ_ADV conj_ADV_PROPN cc_CCONJ_ADV punct_PUNCT_PROPN advmod_ADV_PUNCT case_ADP_ADJ advmod_ADV_PUNCT conj_ADV_PROPN amod_PROPN_PRON appos_PROPN_PROPN flat_PROPN_PROPN punct_PUNCT_PROPN
'''
def makeDepRelSentences(conllufilepath):
    fh =  open(conllufilepath)
    wanted_features = []
    deprels_sentence = []
    sent_id = 0
    head_ids_sentence = []
    pos_tags_sentence = []
    wanted_sentence_form = []
    id_dict = {} #Key: Index, Value: Word or POS depending on what dependency trigram we need. I am taking POS for now.
    id_dict['0'] = "ROOT"
    for line in fh:
        if line == "\n":
            for rel in deprels_sentence:
                wanted = rel + "_" + pos_tags_sentence[deprels_sentence.index(rel)] + "_" +id_dict[head_ids_sentence[deprels_sentence.index(rel)]]
                wanted_sentence_form.append(wanted)
                #Trigrams of the form case_ADP_PROPN, flat_PROPN_PROPN etc.
            wanted_features.append(" ".join(wanted_sentence_form) + "\n")
            deprels_sentence = []
            pos_tags_sentence = []
            head_ids_sentence = []
            wanted_sentence_form = []
            sent_id = sent_id+1
            id_dict = {}
            id_dict['0'] = "root" #LOWERCASING. Some problem with case of features in vectorizer.

        elif not line.startswith("#") and "-" not in line.split("\t")[0]:
            fields = line.split("\t")
            pos_tag = fields[3]
            deprels_sentence.append(fields[7])
            id_dict[fields[0]] = pos_tag
            pos_tags_sentence.append(pos_tag)
            head_ids_sentence.append(fields[6])
    fh.close()
    return " ".join(wanted_features)


"""
As described in Lu, 2010: http://onlinelibrary.wiley.com/doi/10.1111/j.1540-4781.2011.01232_1.x/epdf
Lexical words (N_lex: all open-class category words in UD (ADJ, ADV, INTJ, NOUN, PROPN, VERB)
All words (N)
Lex.Density = N_lex/N
Lex. Variation = Uniq_Lex/N_Lex
Type-Token Ratio = Uniq_words/N
Verb Variation = Uniq_Verb/N_verb
Noun Variation
ADJ variation
ADV variation
Modifier variation
"""
def getLexFeatures(conllufilepath,lang, err):
    fh =  open(conllufilepath)
    ndw = [] #To get number of distinct words
    ndn = [] #To get number of distinct nouns - includes propn
    ndv = [] #To get number of distinct verbs
    ndadj = []
    ndadv = []
    ndint = []
    numN = 0.0 #INCL PROPN
    numV = 0.0
    numI = 0.0 #INTJ
    numADJ = 0.0
    numADV = 0.0
    numIntj = 0.0
    total = 0.0
    numSent = 0.0
    for line in fh:
        if not line == "\n" and not line.startswith("#"):
            fields = line.split("\t")
            word = fields[1]
            pos_tag = fields[3]
            if word.isalpha():
                if not word in ndw:
                    ndw.append(word)
                if pos_tag == "NOUN" or pos_tag == "PROPN":
                    numN = numN +1
                    if not word in ndn:
                        ndn.append(word)
                elif pos_tag == "ADJ":
                    numADJ = numADJ+1
                    if not word in ndadj:
                        ndadj.append(word)
                elif pos_tag == "ADV":
                    numADV = numADV+1
                    if not word in ndadv:
                        ndadv.append(word)
                elif pos_tag == "VERB":
                    numV = numV+1
                    if not word in ndv:
                        ndv.append(word)
                elif pos_tag == "INTJ":
                    numI = numI +1
                    if not word in ndint:
                        ndint.append(word)
        elif line == "\n":
            numSent = numSent +1
        total = total +1
    if err:
        try:
            error_features = getErrorFeatures(conllufilepath,lang)
        #except: 
        except Exception:
            traceback.print_exc()
            print("Ignoring file:",conllufilepath)
            error_features = [0,0]
    else:
        error_features = ['NA', 'NA']

    nlex = float(numN + numV + numADJ + numADV + numI) #Total Lexical words i.e., tokens
    dlex = float(len(ndn) + len(ndv) + len(ndadj) + len(ndadv) + len(ndint)) #Distinct Lexical words i.e., types
    #Scriptlen, Mean Sent Len, TTR, LexD, LexVar, VVar, NVar, AdjVar, AdvVar, ModVar, Total_Errors, Total Spelling errors
    result = [total, round(total/numSent,2), round(len(ndw)/total,2), round(nlex/total,2), round(dlex/nlex,2), round(len(ndv)/nlex,2), round(len(ndn)/nlex,2),
              round(len(ndadj)/nlex,2), round(len(ndadv)/nlex,2), round((len(ndadj) + len(ndadv))/nlex,2),error_features[0], error_features[1]]
    if not err: #remove last two features - they are error features which are NA for cz
       return result[:-2]
    else:
       return result

"""
Num. Errors. NumSpellErrors
May be other error based features can be added later.
"""
def getErrorFeatures(conllufilepath, lang):
    numerr = 0
    numspellerr = 0
    try:
        checker = language_check.LanguageTool(lang)
        text = makeTextOnly(conllufilepath)
        matches = checker.check(text)
        for match in matches:
            if not match.locqualityissuetype == "whitespace":
                numerr = numerr +1
                if match.locqualityissuetype == "typographical" or match.locqualityissuetype == "misspelling":
                    numspellerr = numspellerr +1
    #except:
    except Exception as e:
        print(e)
        print("Ignoring this text: ", conllufilepath)
       # numerr = np.nan
       # numspellerr = np.nan

    return [numerr, numspellerr]


"""
get features that are typically used in scoring models using getErrorFeatures and getLexFeatures functions.
err - indicates whether or not error features should be extracted. Boolean. True/False
"""
def getScoringFeatures(dirpath,lang,err):
    file=open(os.path.join(dirpath,sys.argv[1]+".ord"))
    files= file.readlines()
    fileslist = []
    featureslist = []
    for filename in files:
        filename=filename.rstrip()
        if filename.endswith(".txt"):
            features_for_this_file = getLexFeatures(os.path.join(dirpath,filename),lang,err)
            fileslist.append(filename)
            featureslist.append(features_for_this_file)
    return fileslist, featureslist


"""
Function to get n-gram like features for Word, POS, and Dependency representations
option takes: word, pos, dep. default is word
"""
def getLangData(dirpath, option):
    file=open(os.path.join(dirpath,sys.argv[1]+".ord")) 
    files= file.readlines()
    fileslist = []
    posversionslist = []
    for filename in files:
        filename=filename.rstrip()
        if filename.endswith(".txt"):
            if option == "pos":
            	pos_version_of_file = makePOSsentences(os.path.join(dirpath,filename)) #DO THIS TO GET POS N-GRAM FEATURES later
            elif option == "dep":
                pos_version_of_file = makeDepRelSentences(os.path.join(dirpath,filename)) #DO THIS TO GET DEP-TRIAD N-gram features later
            else:
                pos_version_of_file = makeTextOnly(os.path.join(dirpath,filename)) #DO THIS TO GET Word N-gram features later
            fileslist.append(filename)
            posversionslist.append(pos_version_of_file)
    return fileslist, posversionslist

#Get categories from filenames  -Classification
def getcatlist(filenameslist):
    result = []
    for name in filenameslist:
        #result.append(name.split("_")[3].split(".txt")[0])
        result.append(name.split(".txt")[0].split("_")[-1])
    return result

#Get langs list from filenames - to use in megadataset classification
def getlangslist(filenameslist):
    result = []
    for name in filenameslist:
        if "_DE_" in name:
           result.append("de")
        elif "_IT_" in name:
           result.append("it")
        else:
           result.append("cz")
    return result

#Get numbers from filenames - Regression
def getnumlist(filenameslist):
    result = []
    mapping = {"A1":1, "A2":2, "B1":3, "B2":4, "C1":5, "C2":6}
    for name in filenameslist:
        #result.append(mapping[name.split("_")[3].split(".txt")[0]])
        result.append(mapping[name.split(".txt")[0].split("_")[-1]])
    return result

#Training on one language data, Stratified 10 fold CV
def train_onelang_classification(train_labels,train_data,langfiles,labelascat=False, langslist=None):
    global tfn
    uni_to_tri_vectorizer =  CountVectorizer(analyzer = "word", tokenizer = None, preprocessor = None, stop_words = None, ngram_range=(1,5), min_df=10)
    vectorizers = [uni_to_tri_vectorizer]
    classifiers = [RandomForestClassifier(class_weight="balanced", n_estimators=300, random_state=seed,n_jobs=1), LinearSVC(class_weight="balanced",random_state=seed), LogisticRegression(class_weight="balanced",random_state=seed, multi_class='ovr', n_jobs=1,solver='liblinear')	] #Add more.GradientBoostingClassifier(),
    k_fold = StratifiedKFold(10,random_state=seed)
    pourout = [s.strip(".txt.parsed.txt") for s in langfiles]
    pourout = np.vstack([pourout, train_labels])
    for vectorizer in vectorizers:
        for classifier in classifiers:
            train_vector = vectorizer.fit_transform(train_data).toarray()
            if labelascat and len(langslist) > 1:
               train_vector = enhance_features_withcat(train_vector,language=None,langslist=langslist)
            cross_val = cross_val_score(classifier, train_vector, train_labels, cv=k_fold, n_jobs=1)
            predicted = cross_val_predict(classifier, train_vector, train_labels, cv=k_fold, n_jobs=1)
            pourout = np.vstack([pourout, predicted])           
        with open(tfn, "wb") as fp:
            pickle.dump(pourout.T, fp)
    print("SAME LANG EVAL DONE FOR THIS LANG")
    
#Single language, 10 fold cv for domain features - i.e., non n-gram features.
def singleLangClassificationWithoutVectorizer(train_vector,train_labels,langfiles): #test_vector,test_labels):
    global tfn
    k_fold = StratifiedKFold(10,random_state=seed)
    print(k_fold)
    classifiers = [RandomForestClassifier(class_weight="balanced", n_estimators=300, random_state=seed,n_jobs=1), LinearSVC(class_weight="balanced",random_state=seed), LogisticRegression(class_weight="balanced",random_state=seed, multi_class='ovr', n_jobs=1,solver='liblinear')	] #Add more later
    pourout = [s.strip(".txt.parsed.txt") for s in langfiles]
    pourout = np.vstack([pourout, train_labels])
    for classifier in classifiers:
        print(classifier)
        cross_val = cross_val_score(classifier, train_vector, train_labels, cv=k_fold, n_jobs=1)
        predicted = cross_val_predict(classifier, train_vector, train_labels, cv=k_fold)
        pourout = np.vstack([pourout, predicted])          
    with open(tfn, "wb") as fp:
        pickle.dump(pourout.T, fp)

#add label features as one hot vector. de - 1 0 0, it - 0 1 0, cz - 0 0 1 as sklearn has issues with combination of cat and num features.
def enhance_features_withcat(features,language=None,langslist=None):
   addition = {'de':[1,0,0], 'it': [0,1,0], 'cz': [0,0,1]}
   if language:
        for i in range(0,len(features)):
           features[i].extend(addition[language])
        return features
   if langslist:
        features = np.ndarray.tolist(features)
        for i in range(0,len(features)):
           features[i].extend(addition[langslist[i]])
        return features

"""
Goal: combine all languages data into one big model
setting options: pos, dep, domain
labelascat = true, false (to indicate whether to add label as a categorical feature)
"""
def do_mega_multilingual_model_all_features(lang1path,lang1,lang2path,lang2,lang3path,lang3,modelas, setting,labelascat):
   print("Doing: take all data as if it belongs to one large dataset, and do classification")   
   if not setting == "domain":
      lang1files,lang1features = getLangData(lang1path,setting)
      lang1labels = getnumlist(lang1files)
      lang2files,lang2features = getLangData(lang2path,setting)
      lang2labels = getnumlist(lang2files)
      lang3files,lang3features = getLangData(lang3path,setting)
      lang3labels = getnumlist(lang3files)

   else: #i.e., domain features only.
      lang1files,lang1features = getScoringFeatures(lang1path,lang1,False)
      lang1labels = getnumlist(lang1files)
      lang2files,lang2features = getScoringFeatures(lang2path,lang2,False)
      lang2labels = getnumlist(lang2files)
      lang3files,lang3features = getScoringFeatures(lang3path,lang3,False)
      lang3labels = getnumlist(lang3files)

   megalabels = []
   megalabels = lang1labels + lang2labels + lang3labels
   megalangs = getlangslist(lang1files) + getlangslist(lang2files) + getlangslist(lang3files)
   megafile=lang1files + lang2files + lang3files
   if labelascat and setting == "domain": 
      megadata = enhance_features_withcat(lang1features,"de") + enhance_features_withcat(lang2features,"it") + enhance_features_withcat(lang3features,"cz")
   else:
      megadata = lang1features + lang2features + lang3features
   
   if setting == "domain":
      singleLangClassificationWithoutVectorizer(megadata,megalabels,megafile)
   else:
      train_onelang_classification(megalabels,megadata,megafile,labelascat,megalangs)

     
def main():
    global tfn
    itdirpath = "input/IT-Parsed"
    dedirpath = "input/DE-Parsed"
    czdirpath = "input/CZ-Parsed"
    
    tfn="output/datasets/T2_FA_" + sys.argv[1] + "_W.bin"
    do_mega_multilingual_model_all_features(dedirpath,"de",itdirpath,"it",czdirpath,"cz","class", "word", False)
    tfn="output/datasets/T2_FA_" + sys.argv[1] + "_P.bin"
    do_mega_multilingual_model_all_features(dedirpath,"de",itdirpath,"it",czdirpath,"cz","class", "pos", False)
    tfn="output/datasets/T2_FA_" + sys.argv[1] + "_D.bin"
    do_mega_multilingual_model_all_features(dedirpath,"de",itdirpath,"it",czdirpath,"cz","class", "dep", False)
    tfn="output/datasets/T2_FA_" + sys.argv[1] + "_o.bin"
    do_mega_multilingual_model_all_features(dedirpath,"de",itdirpath,"it",czdirpath,"cz","class", "domain", False)
    tfn="output/datasets/T2_TR_" + sys.argv[1] + "_W.bin"
    do_mega_multilingual_model_all_features(dedirpath,"de",itdirpath,"it",czdirpath,"cz","class", "word", True)
    tfn="output/datasets/T2_TR_" + sys.argv[1] + "_P.bin"
    do_mega_multilingual_model_all_features(dedirpath,"de",itdirpath,"it",czdirpath,"cz","class", "pos", True)
    tfn="output/datasets/T2_TR_" + sys.argv[1] + "_D.bin"
    do_mega_multilingual_model_all_features(dedirpath,"de",itdirpath,"it",czdirpath,"cz","class", "dep", True)
    tfn="output/datasets/T2_TR_" + sys.argv[1] + "_o.bin"
    do_mega_multilingual_model_all_features(dedirpath,"de",itdirpath,"it",czdirpath,"cz","class", "domain", True)
    
if __name__ == "__main__":
    main()
